<?php
namespace Magento\Quote\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Quote\Api\Data\CartInterface
 */
interface CartExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Magento\Quote\Api\Data\ShippingAssignmentInterface[]|null
     */
    public function getShippingAssignments();

    /**
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface[] $shippingAssignments
     * @return $this
     */
    public function setShippingAssignments($shippingAssignments);

    /**
     * @return string|null
     */
    public function getAwOrderNote();

    /**
     * @param string $awOrderNote
     * @return $this
     */
    public function setAwOrderNote($awOrderNote);

    /**
     * @return string|null
     */
    public function getAwDeliveryDate();

    /**
     * @param string $awDeliveryDate
     * @return $this
     */
    public function setAwDeliveryDate($awDeliveryDate);

    /**
     * @return string|null
     */
    public function getAwDeliveryDateFrom();

    /**
     * @param string $awDeliveryDateFrom
     * @return $this
     */
    public function setAwDeliveryDateFrom($awDeliveryDateFrom);

    /**
     * @return string|null
     */
    public function getAwDeliveryDateTo();

    /**
     * @param string $awDeliveryDateTo
     * @return $this
     */
    public function setAwDeliveryDateTo($awDeliveryDateTo);
}
