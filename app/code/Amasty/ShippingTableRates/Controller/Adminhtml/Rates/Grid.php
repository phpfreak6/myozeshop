<?php

namespace Amasty\ShippingTableRates\Controller\Adminhtml\Rates;

class Grid extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
