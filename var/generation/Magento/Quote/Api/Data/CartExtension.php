<?php
namespace Magento\Quote\Api\Data;

/**
 * Extension class for @see \Magento\Quote\Api\Data\CartInterface
 */
class CartExtension extends \Magento\Framework\Api\AbstractSimpleObject implements \Magento\Quote\Api\Data\CartExtensionInterface
{
    /**
     * @return \Magento\Quote\Api\Data\ShippingAssignmentInterface[]|null
     */
    public function getShippingAssignments()
    {
        return $this->_get('shipping_assignments');
    }

    /**
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface[] $shippingAssignments
     * @return $this
     */
    public function setShippingAssignments($shippingAssignments)
    {
        $this->setData('shipping_assignments', $shippingAssignments);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAwOrderNote()
    {
        return $this->_get('aw_order_note');
    }

    /**
     * @param string $awOrderNote
     * @return $this
     */
    public function setAwOrderNote($awOrderNote)
    {
        $this->setData('aw_order_note', $awOrderNote);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAwDeliveryDate()
    {
        return $this->_get('aw_delivery_date');
    }

    /**
     * @param string $awDeliveryDate
     * @return $this
     */
    public function setAwDeliveryDate($awDeliveryDate)
    {
        $this->setData('aw_delivery_date', $awDeliveryDate);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAwDeliveryDateFrom()
    {
        return $this->_get('aw_delivery_date_from');
    }

    /**
     * @param string $awDeliveryDateFrom
     * @return $this
     */
    public function setAwDeliveryDateFrom($awDeliveryDateFrom)
    {
        $this->setData('aw_delivery_date_from', $awDeliveryDateFrom);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAwDeliveryDateTo()
    {
        return $this->_get('aw_delivery_date_to');
    }

    /**
     * @param string $awDeliveryDateTo
     * @return $this
     */
    public function setAwDeliveryDateTo($awDeliveryDateTo)
    {
        $this->setData('aw_delivery_date_to', $awDeliveryDateTo);
        return $this;
    }
}
