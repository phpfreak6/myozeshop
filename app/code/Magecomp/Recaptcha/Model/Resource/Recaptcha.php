<?php
namespace Magecomp\Recaptcha\Model\Resource;
class Recaptcha extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	protected function _construct()
    {
		$this->_init('recaptcha', 'contact_id'); //table name , table primaary key (ID)
    }
}