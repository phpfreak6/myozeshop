<?php

namespace Mageplaza\GoogleTagManager\Helper;

use Mageplaza\Core\Helper\AbstractData;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractData
{
    const XML_PATH_GENERAL_ENABLED = 'googletagmanager/general/is_enabled';
    const XML_PATH_GENERAL_TAG_ID = 'googletagmanager/general/tag_id';
    protected $storeManager;
    protected $objectManager;

    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager  = $storeManager;
        parent::__construct($context, $objectManager, $storeManager);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
    public function isEnabled($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GENERAL_ENABLED, $storeId);

    }
    public function getTagId($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GENERAL_TAG_ID, $storeId);

    }
}