<?php

namespace Amasty\ShippingTableRates\Model\Config\Source;

class Configurable implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $vals = array(
            '0' => __('From associated products'),
            '1'   => __('From parent product'),
        );

        $options = array();
        foreach ($vals as $k => $v)
            $options[] = array(
                    'value' => $k,
                    'label' => $v
            );
        
        return $options;
    }
}
