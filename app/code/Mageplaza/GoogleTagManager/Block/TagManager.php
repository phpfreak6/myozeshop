<?php

namespace Mageplaza\GoogleTagManager\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mageplaza\GoogleTagManager\Helper\Data as HelperData;
use Magento\Cookie\Helper\Cookie as HelperCookie;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class TagManager extends Template
{
    protected $storeManager;
    protected $helperData;
    protected $helperCookie;
    protected $objectFactory;

    public function __construct(
        Context $context,
        HelperData $helperData,
        HelperCookie $helperCookie,
        ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->helperData    = $helperData;
        $this->helperCookie  = $helperCookie;
        $this->objectManager = $objectManager;
        $this->storeManager  = $context->getStoreManager();
        parent::__construct($context, $data);
    }

    public function getCurrentCurrency()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }

    public function getTagId($storeId=null)
    {
        return $this->helperData->getTagId($storeId);
    }

    public function checkConditionShowPage()
    {
        if (!$this->helperCookie->isUserNotAllowSaveCookie() && $this->helperData->isEnabled()) {
            return true;
        }
        return false;
    }

    public function getPageInfo()
    {
        $data    = array();
        $data['ecommerce']['currencyCode']=$this->getCurrentCurrency();
        $request = $this->getRequest();
        $page    = $request->getFullActionName();
        if ($page == 'catalog_category_view') {
            $data['pageCategory'] = 'catalog-listing';
        } elseif ($page == 'catalog_product_view') {
            $data['pageCategory'] = 'product-details';
        } elseif ($page == 'customer_product_view') {
            $data['pageCategory'] = 'customer-login';
        }
        return $data;
    }
}