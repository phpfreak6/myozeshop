<?php
namespace Magecomp\Recaptcha\Controller\Index;
class Index extends \Magento\Contact\Controller\Index
{
	const XML_PATH_EMAIL_RECIPIENT = 'contact/email/recipient_email';
	
	public function execute()
	{
		$ccode = $this->getRequest()->getPost('g-recaptcha-response');
		$cname = $this->getRequest()->getPost('name');
   		$cemail = $this->getRequest()->getPost('email');
   		$cmob = $this->getRequest()->getPost('telephone');
        $comment = $this->getRequest()->getPost('comment');	
	    $comparecode = substr($ccode,0,5);
		if($comparecode == '03AJz')
		{
			$post = $this->getRequest()->getPostValue();
			$block = $this->_view->getLayout()->createBlock('\Magecomp\Recaptcha\Block\Recaptcha');
			$block->insertItem($cname,$cemail,$cmob,$comment);
			// Email Sending code//
			$this->inlineTranslation->suspend();
			try {
				$postObject = new \Magento\Framework\DataObject();
				$postObject->setData($post);
				$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
				$transport = $this->_transportBuilder
					->setTemplateIdentifier('send_email_email_template')
					->setTemplateOptions(
					[
						'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
						'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
					]
					)
					->setTemplateVars(['data' => $postObject])
					->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
					->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope))
					->setReplyTo($post['email'])
					->getTransport();
		
					$transport->sendMessage();
					$this->inlineTranslation->resume();
					$this->_redirect('contact/index?success=true');
					return;
				}
				catch(\Exception $e) {
					$this->inlineTranslation->resume();
					$this->_redirect('contact/index?success=false');
					return;
				}              
		}
		if($cemail != '' && $comparecode != '03AJz'){
			$this->_redirect('contact/index?captcha=false');
			return;
		}else{
			$this->_view->loadLayout();
        	$this->_view->getLayout()->initMessages();
            $this->_view->renderLayout();	 
		}
    }
}
